<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
    <h1>TODOS</h1>
    <h2>Global</h2>
    <ul>
        <#list toto as unTodo>
            <li>Name : ${unTodo.getName()} IP ${unTodo.ip} Nb : ${unTodo.nbr}</li>
        </#list>
    </ul>
    <h2>Session</h2>
    <ul>
        <#list tata as unTodo>
            <li>Name : ${unTodo.name} IP ${unTodo.ip} Nb : ${unTodo.nbr}</li>
        </#list>
    </ul>
    <h2>Cookie</h2>

    <ul>
    <#list titi as unTodo>
        <li>Name : ${unTodo.name} IP ${unTodo.ip} Nb : ${unTodo.nbr}</li>
    </#list>
    </ul>
    <form action="/processTodo" method="post" name="todoData">
        <label for="">Nom du todo</label>
        <input name="name" type="text">
        <label for="">Global :</label>
        <input type="radio" name="choix" value="global" id="">
        <label for="">Session</label>
        <input type="radio" name="choix" value="local" id="">
        <label for="">Cookie</label>
        <input type="radio" name="choix" value="cookie" id="">
        <button>Envoyer</button>
    </form>
</body>
</html>