<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
    <h1>Welcome</h1>
    <ul>
        <#list jeuxTemplate as unJeu>
        <li>Name : ${unJeu.name} Number of players : ${unJeu.numberOfPlayers} Type : ${unJeu.type} </li>
        </#list>
    </ul>
</body>
</html>