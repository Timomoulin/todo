package com.greta.todo.controllers;

import com.greta.todo.models.Game;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import java.util.ArrayList;
import java.util.List;

@Controller
public class GameController {

    List<Game> globalGames=new ArrayList<Game>();

    @GetMapping(value = "/games")
    public String games (Model model){
        List<Game> jeux= this.globalGames;
        model.addAttribute("jeuxTemplate", jeux);
        return "listGames";
    }

    @PostMapping
    public String processGame(@ModelAttribute(name = "")Game newGame){

    }
}
