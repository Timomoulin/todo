package com.greta.todo.controllers;

import com.greta.todo.dto.TodoDTO;
import com.greta.todo.models.Todo;
import com.greta.todo.services.TodoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.net.HttpCookie;
import java.net.http.HttpRequest;
import java.util.ArrayList;
import java.util.List;

@Controller
public class TodoController {
    @Autowired
    private TodoService todoService;


    @GetMapping(value = "/todo")
    public String home(Model model, HttpServletRequest httpServletRequest) {
        //creation d'une variable todoGlobal a laquelle on affecte la liste des todos global ( l'attribut de TodoService)
        List<Todo> todoGlobal = this.todoService.getTodosGlobal();
        //Simple : j'ajoute au model un attribut toto qui pour valeur todoGlobal
        //J'invoque la methode addAttribute pour ajouter un attribut au model ...
        //<b> Les donées du model seront utilisable dans un template</b>
        model.addAttribute("toto", todoGlobal);

        //Recupere la session "la super globale session $_SESSION"
        HttpSession session = httpServletRequest.getSession();
        // On recupere $todoSession=$_SESSION["todoSession"]
        List<Todo> todoSession = (List<Todo>) session.getAttribute("todoSession");
        if (todoSession == null) {
            todoSession = new ArrayList<Todo>();
            session.setAttribute("todoSession", todoSession);
        }
        model.addAttribute("tata", todoSession);

        Cookie[] cookies = httpServletRequest.getCookies();
        Cookie cookie = null;
        List<Todo> todoCookie = new ArrayList<Todo>();
        for (Cookie unCookie : cookies
        ) {
            if (unCookie.getName().equals("todoCookie")) {
                cookie = unCookie;
            }
        }
        if (cookie != null) {
            todoCookie = todoService.stringToListTodo(cookie.getValue());
        }
        model.addAttribute("titi", todoCookie);

        // la string retourner corespond au nom du template (sauf si redirect)
        return "listeTodo";
    }

    @PostMapping(value = "/processTodo")
    public String process(@ModelAttribute(name = "todoData") TodoDTO newTodoDTO, HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) {

        //Recupere le bouton radio du form/DTO
        //String choix= httpServletRequest.getParameter("choix");
        String choix = newTodoDTO.getChoix();
        //Convertir le DTO en Todo
        //on invoque la méthode convertir depuis le service et on lui donne en arguments :
        // le DTO et l'adresse ip de la personne qui a émit la requete
        Todo nouveauTodo = this.todoService.convertir(newTodoDTO, httpServletRequest.getRemoteAddr());

        if (choix.equals("global")) {
            //Simple : creation d'une variable newGlobal qui corespont a la liste de todoglobal + le nouveau todo
            //creation d'une variable qui pour valeur le resultat de la méthode supplement a laquelle on fournit en argument
            // la liste de todoglobal et le nouveau tod
            List<Todo> newGlobal = this.todoService.supplement(todoService.getTodosGlobal(), nouveauTodo);
            //on remplace les todo global par newGlobal
            todoService.setTodosGlobal(newGlobal);
        } else if (choix.equals("local")) {
//            //Recupere la session "la super globale session $_SESSION"
            HttpSession session = httpServletRequest.getSession();
//           // On recupere $todoSession=$_SESSION["todoSession"]
            List<Todo> todoSession = (List<Todo>) session.getAttribute("todoSession");
            //Si il y a rien dans la session (pas d'attribut todoSession)
            if (todoSession == null) {
                //alors la variable todoSession a pour valeur un arraylist vide
                todoSession = new ArrayList<Todo>();
            }
            //On ecrase todoSession par le resultat de supplement cad :
            // son ancienne valeur avec le nouveauTodo
            todoSession = todoService.supplement(todoSession, nouveauTodo);
            //On enregistre la nouvelle valeur de todoSession dans la session
            //$_SESSION["todoSession"]=todoSession;
            session.setAttribute("todoSession", todoSession);

        } else if (choix.equals("cookie")) {

            //ici on recup les cookie
            Cookie[] cookies = httpServletRequest.getCookies();
            //on créer deux variable une pour le cooki l'autre pour la liste de Todo des cookies
            Cookie todoCookie = null;
            List<Todo> listTodoCookie = new ArrayList<Todo>();
            //Verification que il y a un cookie qui pour nom todoCookie
            for (Cookie cookie : cookies
            ) {
                if (cookie.getName().equals("todoCookie")) {
                    todoCookie = cookie;
                }
            }
            //Si il y a un todoCookie on affecte a listTodoCookie la valeur du cookie (en String) convertie en List<Todo>
            if (todoCookie != null) {
                listTodoCookie = this.todoService.stringToListTodo(todoCookie.getValue());
            }
            //On ajoute le nouveau todo a la liste
            listTodoCookie = this.todoService.supplement(listTodoCookie, nouveauTodo);
            // on convertie la liste de Todo en string
            String stringCookie = this.todoService.listTodoToString(listTodoCookie);
            //creation d'un cookie
            todoCookie = new Cookie("todoCookie", stringCookie);
            //on ajoute le cookie a la reponse
            httpServletResponse.addCookie(todoCookie);
        }
        return "redirect:/todo";
    }
}
