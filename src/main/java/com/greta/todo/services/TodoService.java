package com.greta.todo.services;

import com.greta.todo.dto.TodoDTO;
import com.greta.todo.models.Todo;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class TodoService {
    private List<Todo> todosGlobal = new ArrayList<Todo>();

    /**
     * Ajouter un <b>todo</b> a la liste de todo
     * @param {Todo} newTodo
     */
    public void ajouter(Todo newTodo,List<Todo> todoList){
        todoList.add(newTodo);
    }

    /**
     * Ajoute un Todo a une liste ou incremente le number du Todo dans la liste
     * @param listTodo
     * @param newTodo
     * @return
     */
    public List<Todo> supplement (List<Todo> listTodo,Todo newTodo){
        if(listTodo.contains(newTodo)){
            for (Todo unTodo:listTodo){
                if(unTodo.equals(newTodo)){
                    unTodo.setNbr(unTodo.getNbr()+1);
                }
            }
        }
        else{
           ajouter(newTodo,listTodo);
        }
        return listTodo;
    }

    /**
     * Convertir un DTO en Todo
     * @param todoDTO
     * @param unIp
     * @return
     */
    public Todo convertir (TodoDTO todoDTO,String unIp){
        Todo resultat= new Todo(todoDTO.getName(),unIp);
        return resultat;
    }

    /**
     * Convertir une string en un Todo
     * @param {String} string
     * @return {Todo}
     */
    public Todo stringToTodo (String string){
        String[] tableau=string.split("-");
        return  new Todo(tableau[0],tableau[1],Integer.parseInt(tableau[2]));
    }


    /**
     * Convertir une String en List de Todo
     * @param {String} string une string
     * @return {List<Todo>} Une liste de Todo
     */
    public List<Todo> stringToListTodo(String string){
        List<Todo> resultat=new ArrayList<Todo>();
        String[] stringTodos= string.split("/");
        for (String chaineTodo:stringTodos
             ) {
            resultat.add(this.stringToTodo(chaineTodo));
        }
        return resultat;
    }

    /**
     * Convertir un Todo en string
     * @param {Todo} unTodo objet Todo
     * @return {String} une string
     */
    public String todoToString(Todo unTodo){
        return unTodo.getName()+"-"+unTodo.getIp()+"-"+unTodo.getNbr();
    }

    /**
     * Convertir une liste de Todo en String
     * @param {List<Todo>} todos la liste de todo
     * @return {String} une string
     */
    public String listTodoToString(List<Todo> todos){
        String resultat="";
        for (Todo unTodo:todos
             ) {
            resultat+=this.todoToString(unTodo)+"/";
        }
        return  resultat;
    }

    public List<Todo> getTodosGlobal() {
        return todosGlobal;
    }

    public void setTodosGlobal(List<Todo> todosGlobal) {
        this.todosGlobal = todosGlobal;
    }


}
