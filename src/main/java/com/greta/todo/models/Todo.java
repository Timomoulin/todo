package com.greta.todo.models;

import lombok.*;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Getter
@Setter
public class Todo {

    private long id;
    private String name;
    private String ip;
    private int nbr=1;

    public Todo() {
    }

    public Todo(String name, String ip) {
        this.name = name;
        this.ip = ip;
    }

    public Todo(String name, String ip, int nbr) {
        this.name = name;
        this.ip = ip;
        this.nbr = nbr;
    }
/*
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public int getNbr() {
        return nbr;
    }

    public void setNbr(int nbr) {
        this.nbr = nbr;
    }
*/

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Todo todo = (Todo) o;

        return name.equals(todo.name);
    }

    @Override
    public int hashCode() {
        return name.hashCode();
    }
}
