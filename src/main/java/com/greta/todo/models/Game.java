package com.greta.todo.models;

public class Game {
    private String name;
    private String type;
    private int numberOfPlayers;

    public Game() {
    }

    public Game(String name, String type, int numberOfPlayers) {
        this.name = name;
        this.type = type;
        this.numberOfPlayers = numberOfPlayers;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getNumberOfPlayers() {
        return numberOfPlayers;
    }

    public void setNumberOfPlayers(int numberOfPlayers) {
        this.numberOfPlayers = numberOfPlayers;
    }


}
