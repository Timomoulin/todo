package com.greta.todo.dto;

public class TodoDTO {
    private String name ;
    private String choix;

    public TodoDTO() {
    }

    public TodoDTO(String name, String choix) {
        this.name = name;
        this.choix = choix;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getChoix() {
        return choix;
    }

    public void setChoix(String choix) {
        this.choix = choix;
    }
}
